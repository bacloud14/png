import Dropzone from 'dropzone'
import ExifReader from 'exifreader'
import 'dropzone/dist/dropzone.css'
import 'leaflet/dist/leaflet.css'
import markerImage from './assets/mark.svg' // Import the marker image

import * as L from 'leaflet'

const myDropzone = new Dropzone('#my-form')
const output = document.querySelector('#output')
const imageMod = document.querySelector('.image')
let map
const mapContainer = document.querySelector('#map_container')

function getDescription(tag) {
    if (Array.isArray(tag)) {
        return tag.map((item) => item.description).join(', ')
    }
    return tag.description
}

// this transform gps data to feed into the map

/**
 *
 * @param {ExifReader.ExifTags} obj
 */

const getGpsFormat = (obj) => {
    return [obj.GPSLatitude, obj.GPSLongitude]
}

/**
 *
 * @param {Dropzone.DropzoneFile} file
 */

// a custome marker
var myIcon = L.icon({
    iconUrl: markerImage,
    iconSize: [40, 40],
    iconAnchor: [12, 60],
    popupAnchor: [-3, -76],
})

window.addEventListener('load', () => {
    map = L.map('map', {
        center: { lat: 51, lng: -0.09 },
        zoom: 12,
    })
    map.invalidateSize()
    // L.marker([51.505, -0.09], { icon: myIcon }).addTo(map)

    L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution:
            '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
        detectRetina: true,
    }).addTo(map)
})

/**
 *
 * @param {ExifReader.NumberArrayTag[]} gps
 */

// to add a marker at a specific location
const addMarkers = (gps) => {
    console.log(gps)
    L.marker([gps[0].description, gps[1].description], { icon: myIcon })
        .addTo(map)
        .bindPopup('Latitude: '+ gps[0].description + '</br>' + 'Longitude: '+ gps[1].description  +'.')
        .openPopup()
    map.setView([gps[0].description, gps[1].description], 15)
}

myDropzone.on('addedfile', async (file) => {
    const tableBody = document.createElement('table')
    tableBody.className = 'table table-striped table-hover'

    const tags = ExifReader.load(await file.arrayBuffer())

    const tagsExpanded = ExifReader.load(await file.arrayBuffer(), {
        expanded: true,
    })
    // const gps = tagsExpanded.gps  // this doesn't check if gps data exist, so i'm checking directly with exf

    
    if (tagsExpanded.exif.GPSLongitude) {
        let location = getGpsFormat(tagsExpanded.exif)
        addMarkers(location)
    }

    for (const name in tags) {
        let description = getDescription(tags[name])
        if (description !== undefined) {
            let row = document.createElement('tr')
            row.innerHTML = '<td>' + name + '</td><td>' + description + '</td>'
            tableBody.appendChild(row)
        }
    }

    output.insertBefore(tableBody, output.childNodes[0])

    const originalImageUrl = file.dataURL
    // create a new image without the exif data

    stripExifFromImage(originalImageUrl, function (blob) {
        let ima = document.createElement('img')
        ima.classList.add('image')
        ima.src = URL.createObjectURL(blob)

        // add a download button, to download the image

        let downloadBtn = document.createElement('button')
        downloadBtn.classList.add('downloadBtn')
        downloadBtn.innerText = 'Download'

        downloadBtn.addEventListener('click', () => {
            var a = document.createElement('a')
            a.href = ima.src
            a.download = 'output.png'
            document.body.appendChild(a)
            a.click()
            document.body.removeChild(a)
        })

        output.insertBefore(ima, output.childNodes[1])
        output.insertBefore(downloadBtn, output.childNodes[2])
    })

    output.innerHTML =
        `<div><b>File added: ${file.name}<b></div>` + output.innerHTML
})

//write the image into a new canvas

function stripExifFromImage(imageUrl, callback) {
    const img = new Image()

    img.onload = function () {
        // Create a canvas
        const canvas = document.createElement('canvas')
        canvas.width = img.width
        canvas.height = img.height

        const context = canvas.getContext('2d')
        context.drawImage(img, 0, 0)

        canvas.toBlob(callback, 'image/jpeg')
    }

    img.src = imageUrl
}
